## Accompagner l'individu et le collectif

### Niveau 1

- Proposer une activité de remédiation pour des élèves en difficulté
- Proposer une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance
