# Fiche thématique

## Langage SQL

### Le programme officiel

| Contenu                  | Capacités attendues | Commentaires |
| ------------------------ | ------------------- | --------- |
| Langage SQL : requêtes d'interrogation et de mise à jour d'une base de données | Identifier les composants d'une requête. Construire des requêtes d'interrogation à l'aide des clauses du langage SQL :<br>`SELECT`, `FROM`, `WHERE`, `JOIN`<br><br>Construire des requêtes d'insertion et de mise à jour à l'aide de : `UPDATE`, `INSERT`, `DELETE` | On peut utiliser `DISTINCT`, `ORDER BY` ou les fonctions d'agrégation sas utiliser les clauses `GROUP BY` et `HAVING` |