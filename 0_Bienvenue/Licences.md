# Droits et conditions d'utilisation

## Conditions d’utilisation des ressources pédagogiques

Les ressources du cours sont, sauf mention contraire, diffusées sous Licence Creative Commons [CC BY 4.0 : Attribution](https://creativecommons.org/licenses/by/4.0/deed.fr).

Vous êtes autorisé à :

    Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
    Adapter — remixer, transformer et créer à partir du matériel pour toute utilisation, y compris commerciale.

Selon les conditions suivantes :

    Attribution — Vous devez créditer l'oeuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'offrant vous soutient ou soutient la façon dont vous avez utilisé son oeuvre.


## Conditions d’utilisation des contenus produits par les participants

Les contenus produits par les participants sont, sauf mention contraire, diffusés sous Licence Creative Commons [CC BY-NC-SA 4.0 : Attribution + Pas d’Utilisation Commerciale + Partage dans les mêmes conditions](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr). 

Vous êtes autorisé à :

    Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats
    Adapter — remixer, transformer et créer à partir du matériel

Selon les conditions suivantes :

    Attribution — Vous devez créditer l'oeuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'offrant vous soutient ou soutient la façon dont vous avez utilisé son oeuvre.
    Pas d’Utilisation Commerciale — Vous n'êtes pas autorisé à faire un usage commercial de cette oeuvre, tout ou partie du matériel la composant.
    Partage dans les Mêmes Conditions — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'oeuvre originale, vous devez diffuser l'oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'oeuvre originale a été diffusée.
