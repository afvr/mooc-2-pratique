# Mettre en oeuvre et animer son activité

Après avoir choisi son contenu, la question se pose de sa mise en œuvre. 

Décrire, par quelques méta-données, une activité élève est très précieux pour formaliser ce qui va être fait, mais aussi partager ou valider une activité.

Ce n'est évidemment pas suffisant pour mener à bien une séance de cours. Ici, nous nous penchons sur **la mise en oeuvre, en classe, d'une activité**. Nous allons par exemple nous demander :

* Pourquoi faire les étapes dans tel ordre ? 
* Quand donner la parole aux élèves ? 
* Quand la reprendre pour avancer ? 
* Quelles interactions entre les élèves ? 
* Quels sont les passages délicats à bien calculer ? 

On passe donc ici à la mise en place d'un **scénario de séquence en soulevant et anticipant les questions qui vont se poser**.

<div class="styleguide-color" style="background:#fef1ef">
  <p style="margin-left:1em; margin-right:1em;"><br/>
    <b><em> Important :</em></b></p>
  <p style="margin-left:1em; margin-right:1em;">
Notez que quelque soit votre degré de préparation, d’analyse et d’anticipation des événements de votre séance, <b>vous ne pourrez pas tout prévoir</b>. Ce n’est pas grave et vous devez vous attendre à ce qu’une séance même très bien préparée ne se déroule pas comme prévu.</p>
<p></p>
  <p style="padding-bottom:0.5em;"></p>
</div>    


## Objectifs

Les objectifs visés sont :

- À partir d'une activité existante :

  - Identifier et critiquer le scénario d'une séquence
  - Scénariser une séquence existante

- À partir de l'activité que vous avez _créée_ en 2.1 : **scénariser entièrement une séance**, en précisant notamment :

  - les différentes phases, durée,
  - l'organisation du tableau, écran, papier
  - l'organisation pédagogique (seul, binôme, groupe...)
  - les attendus
  - les interactions
  - l'activité du professeur
  - la place de la trace écrite


## Boîte à outils

Pour vous préparer vous aurez à votre disposition : 

- La troisième partie, du livre "Enseigner l'Informatique" de Werner Hartmann, suivi du quiz-bloc2 n°2

- 4 ressources de collègues pour vous inspirer et discuter sur le forum.

- La _to do_ liste de fin, avec les actions à réaliser pour répondre aux objectifs ET pour bien se préparer pour l'évaluation par les pairs.

Et bien sûr la [communauté d’apprentissage et de pratique grâce au forum](https://mooc-forums.inria.fr/moocnsi/c/mooc-2/mettre-en-oeuvre-animer/193) qui fait converger le forum de la communauté CAI (Communauté d'Apprentissage de l'Informatique) et les forums des Moocs NSI.


