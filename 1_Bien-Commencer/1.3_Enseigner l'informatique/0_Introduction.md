## Enseigner l'Informatique

Si la didactique de l'Informatique est une discipline jeune, elle n'est toutefois pas vierge de jalons, de repères que d'autres ont posés. Dans ce Mooc, nous vous proposons des références à l'ouvrage : "Enseigner l'Informatique" par W. Hartmann, M. Näf et R. Reichert, dans lequel, de façon très pragmatique les auteurs illustrent par des exemples ce qu'un enseignement d'Informatique est, ce qu'il n'est pas et ce qu'il ne devrait surtout pas devenir.

Dans une première partie intitulée "Classification et délimitation", l'ouvrage pose le cadre de ce que peut contenir un enseignement d'Informatique et du rôle d'un enseignant d'Informatique :

1. L'objet du cours d'Informatique c'est l'Informatique !
    L'informatique _se voit_ comme :
        - Outil, au quotidien et dans l'enseignement spécialisé
        - Support via les logiciels d'apprentissage et la formation à distance
        - Matière enseignée
2. Les enseignant d'Informatique enseignent l'Informatique...
    et ne sont ni experts dans l'évaluation de supports informatiques pour les besoins des enseignements, ni personnels du service de maintenance

Consulter la Partie I de l'ouvrage : _ici le lien_

Au fil du Mooc, nous mettrons des liens vers les chapitres de l'ouvrage en rapport avec le thème du module.
