**Table des matières**

**Partie I Classification et délimitation**  

1 L'informatique en tant que sujet de l'enseignement informatique
2 Les professeurs d'informatique enseignent l'informatique 
3 Les professeurs d'informatique ne sont pas la Hotline hotline des TCI technologies de l'information et de la communication (TIC)
4 Les professeurs d'informatique n'ont pas la priorité auprès de l'assistance TCIC 
5 Le besoin en formation continue des professeurs d'informatique est très élevé


**Partie II Choix du contenu des cours**  

6 L'enseignement de l'informatique englobe la connaissance des concepts et la connaissance des produits
7 Adapter le contenu des cours au public visé
8 Idées fondamentales


**Partie III Planification de l'enseignement**  

9 Les différents accès à l'enseignement de l'informatique  
10 Objectifs pédagogiques dans l'enseignement de l'informatique  
11 Les cours d'informatique doivent être soigneusement planifiés


**Partie IV Méthodes d'enseignement**  

12 Méthodes pour l'enseignement de l'informatique 
13 Pédagogie expérientielle 
14 Travail en groupe 
15 Programmes dirigés 
16 Apprentissage par la découverte 
17 Pédagogie de projet


**Partie V Techniques d'enseignement**  

18 Les structurants préalables pour en venir à l'essentiel
19 Des abstractions qui deviennent concrètes grâce au tiercé des représentations 
20 Les visualisations pour découvrir l'invisible
21 Lire avant d'écrire


**Partie VI Mise en œuvre de l'enseignement**  

22 Séparer la théorie et la pratique 
23 Distinguer les outils des objets
24 Les professeurs d'informatique ne peuvent pas tout connaître 
25 Travail sur ordinateur : les mains dans le dos ! 
26 Apprendre à gérer ses erreurs
