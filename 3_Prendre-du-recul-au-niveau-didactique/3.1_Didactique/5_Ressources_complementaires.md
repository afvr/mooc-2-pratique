# Ressources complémentaires

## De la recherche internationale en didactique de l'informatique

Au niveau international l'<a href="https://en.wikipedia.org/wiki/Association_for_Computing_Machinery" target="_blank">Association for Computing Machinery's</a> (ACM) Special Interest Group (SIG) on <a href="https://en.wikipedia.org/wiki/Computer_science_education" target="_blank">Computer science education</a> (CSE), rassemble au sein de la <a href="https://en.wikipedia.org/wiki/SIGCSE" target="_blank">SIGCSE</a> les initiatives de recherche, développement, implémentation, et évaluation des ressources pour l'apprentissage de l'informatique.


Via le forum NSI _Louis Leskow_ nous a proposé une sélection :

- <a href="https://dl.acm.org/doi/10.1145/3341525.3387427" target="_blank">Des idées de thèmes d’exercices</a>
- <a href="https://dl.acm.org/doi/10.1145/3141880.3141895" target="_blank">De nouvelles formes d’exercices</a>
- <a href="https://dl.acm.org/doi/10.1145/3287324.3287428" target="_blank">Des exemples de curriculum en pratique avec retours</a>
- <a href="https://dl.acm.org/doi/10.1145/572139.572181" target="_blank">Des choses générales sur les difficultés des apprenants</a>
- <a href="https://dl.acm.org/doi/10.1145/2960310.2960327" target="_blank">Des analyses sur des points particuliers</a>
- <a href="https://dl.acm.org/doi/10.1145/2960310.2960329" target="_blank">Des articles sur ce qui peut aider les filles à s’intéresser et à réussir</a>

<!--Il mentionne aussi [bluej.org/blackbox](https://bluej.org/blackbox) qui est une de partage de données sur les pratiques des élèves/étudiant·e·s.-->

Nous pourrions partager sur ces sujets sur le [forum NSI](https://mooc-forums.inria.fr/moocnsi/t/ressources-pour-la-didactique-de-linformatique/2840)

### Un recueil de ressources sur la didactique de l'informatique

 À l'initiative de <a href="https://researchportal.unamur.be/fr/persons/julie-henry" target="_blank">Julie Henry</a>, voici un recueil de ressources didactiques co-construit par les collègues … profitons-en et … participons y !

 - <a href="https://docs.google.com/document/d/1c_NxE4HONPSxkLJpMmmdKg9RUiB7zpQpbNvzxUPNHHQ/edit" target="_blank">Aspects didactiques de l'informatique</a>
